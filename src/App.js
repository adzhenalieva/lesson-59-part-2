import React, {Component} from 'react';
import Anekdote from "./component/anekdote/Anekdote";
import Button from './component/Button/Button';

import './App.css';


class App extends Component {
    state = {
        text: " "
    };

    componentDidMount() {

        fetch('https://api.chucknorris.io/jokes/random').then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong');
        }).then(anekdotes => {
            let anekdote = anekdotes.value;

            this.setState({text: anekdote})

        }).catch(error => {
            console.log(error);
        });

    }

   generateNewJoke = () => {
      this.componentDidMount()
    };

    render() {
        return (
            <div className="App">
                <Anekdote text={this.state.text} />
                <Button onClick={this.generateNewJoke} />
            </div>
        );
    }
}


export default App;
