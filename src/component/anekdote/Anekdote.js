import React from 'react';
import './Anekdote.css';

const Anekdote = props => {
    return (
        <div>
            <p className="title">The joke from Chuck Norris</p>
            <p>{props.text}</p>
        </div>
    );
};

export default Anekdote;