import React, {Component} from 'react';
import './Button.css';

class Button extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.children !== this.props.children
    }

    render() {
        console.log('update');
        return (
            <button key="123" className="button" onClick={this.props.onClick}>New joke</button>
        );
    }
}

export default Button;